#!/usr/bin/env python  # noqa: EXE001
from __future__ import annotations

from distutils.core import setup

setup(
    name="hmtoinflux",
    version="0.0",
    description="a bridge for sensor data from homematic to influxdb",
    long_description=open("README.md").read(),  # noqa: PLW1514, PTH123, SIM115
    author="David Bauer",
    author_email="hmtoinflux@debauer.net",
    packages=["hmtoinflux"],
)
